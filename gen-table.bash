#!/bin/bash

make
preproc data/ba.5k data/ba.5k.bin > /dev/null

echo "generating table in README..."
echo 

data='./data/ba.5k.bin'
bin='./maxcov'
k='50'
epsi='0.1'
repetitions='100'

printf "%15s %15s %15s\n" "Algorithm" "Objective" "Queries"

#run standard greedy
out=`$bin -g $data -k $k -q -G -e $epsi`
printf "%15s %15d %15d\n" "GREEDY" $out

#run sievestream++
out=`$bin -g $data -k $k -q -S -e $epsi`
printf "%15s %15d %15d\n" "SIEVESTREAM++" $out

#run P-PASS
out=`$bin -g $data -k $k -q -P -e $epsi`
printf "%15s %15d %15.0f\n" "P-PASS" $out

#run LTL
out=`$bin -g $data -k $k -q -L -e $epsi -N $repetitions`
printf "%15s %15.0f %15.0f\n" "LTL" $out

#run C&K
out=`$bin -g $data -k $k -q -K -e $epsi`
printf "%15s %15.0f %15.0f\n" "C&K" $out

#run QS+BR
out=`$bin -g $data -k $k -q -R -e $epsi -c 1`
printf "%15s %15d %15d\n" "QS + BR" $out

#run QuickStream_1
out=`$bin -g $data -k $k -q -Q -e $epsi`
printf "%15s %15d %15d\n" "QuickStream_1" $out

#run QuickStream_1++
out=`$bin -g $data -k $k -q -Q -e $epsi -p`
printf "%15s %15d %15d\n" "QuickStream_1++" $out

#run QuickStream_2
out=`$bin -g $data -k $k -q -Q -e $epsi -c 2`
printf "%15s %15d %15d\n" "QuickStream_2" $out

#run QuickStream_2++
out=`$bin -g $data -k $k -q -Q -e $epsi -p -c 2`
printf "%15s %15d %15d\n" "QuickStream_2++" $out

#run QuickStream_8
out=`$bin -g $data -k $k -q -Q -e $epsi -c 8`
printf "%15s %15d %15d\n" "QuickStream_8" $out

#run QuickStream_8++
out=`$bin -g $data -k $k -q -Q -e $epsi -p -c 8`
printf "%15s %15d %15d\n" "QuickStream_8++" $out




