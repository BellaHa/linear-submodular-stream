## Quick Streaming Algorithms for Maximization of Monotone Submodular Functions in Linear Time

This is the reference implementation used for the empirical evaluation in the paper:

Kuhnle, A. (2021). Quick Streaming Algorithms for Maximization of Monotone Submodular Functions  in Linear Time. In Artificial Intelligence and Statistics (AISTATS).

Full version of the paper is available here: https://arxiv.org/abs/2009.04979

for both the revenue maximization and maximum cover applications.
The implemented algorithms include the algorithms
QuickStream_c, QuickStream_c++, and  QS+BR (introduced in paper above), as
well as the following comparison algorithms:
<ul>
 <li> StandardGreedy, the standard greedy algorithm.
 <li> SieveStream++, as described in

Ehsan Kazemi, Marko Mitrovic, Morteza Zadimoghad-
dam, Silvio Lattanzi, and Amin Karbasi. Submodu-
lar Streaming in All its Glory: Tight Approximation,
Minimum Memory and Low Adaptive Complexity.
In International Conference on Machine Learning
(ICML), 2019.
<li> P-PASS, as described in

Ashkan Norouzi-Fard, Jakub Tarnawski, Slobodan
Mitrovic, Amir Zandieh, Aidasadat Mousavifar, and
Ola Svensson. Beyond 1/2-Approximation for Sub-
modular Maximization on Massive Data Streams.
In International Conference on Machine Learning
(ICML), 2018.
<li> Lazier-Than-Lazy-Greedy (LTL), as described in

Baharan Mirzasoleiman, Ashwinkumar Badanidiyuru,
Amin Karbasi, Jan Vondrak, and Andreas Krause.
Lazier Than Lazy Greedy. In AAAI Conference on
Artificial Intelligence (AAAI), 2015.
<li> C&K, as described in

Amit Chakrabarti and Sagar Kale. Submodular maximization meets streaming: matchings, matroids, and
more. Mathematical Programming, 154(1-2):225–247,
2015.
</ul>	

### Dependencies 
GNU g++, GNU make utility

### Binaries
<ul>
<li> `maxcov`, the main program to run all algorithms on maximum cover
<li> `revmax`, the main program to run all algorithms on revenue max
<li> `preproc`, for preprocessing edge lists to custom binary format
</ul>

### Format of input graph

The programs take as input a custom binary format; the program
`preproc` is provided to convert an undirected edge list format
to the binary format. 

Each line describes an edge
```
<From id> <To id> <Weight (only if weighted)>
```
The node ids must be nonnegative integers; they are not restricted to lie in any range.
The `Weight` must be an unsigned integer.

If `graph.el` is an edge list in the above format, then
```
preproc graph.el graph.bin
```
generates the `graph.bin` file in the correct binary format for input to the other programs.

### Parameters 
```
-g <graph filename in binary format>
-k <cardinality constraint>
-G [run StandardGreedy]
-S [run SieveStream++]
-P [run P-Pass]
-L [run LTL]
-K [run Chakrabarti&Kale]
-Q [run QuickStream]
-R [run QS + BR]
-o <outputFileName>
-N <repetitions>
-e <accuracy parameter epsilon (default 0.1)>
-p [Only with -Q, run QuickStream++]
-t <Tradeoff in error, only applies to P-Pass>
-c <blocksize, only applies to QuickStream(++)>
-q [quiet mode]

```
### Example
```
bash gen-table.bash
```
Should generate the following table, for maxcover
on a Barabasi-Albert random graph
with n = 4997, k = 50.
```
      Algorithm       Objective         Queries
         GREEDY            3865          248831
  SIEVESTREAM++            3380          135138
         P-PASS            3864         1362850
            LTL            3651           11495
            C&K            3809            9996
        QS + BR            3864          104542
  QuickStream_1            1997            4998
QuickStream_1++            3864            5381
  QuickStream_2            1873            2501
QuickStream_2++            3864            2993
  QuickStream_8            3739             632
QuickStream_8++            3864            1477			
```
### Reproduce Experiments in Paper
Scripts are provided to partially reproduce the experiments in the paper,
namely the experiments with small k values for the maximum cover application
on web-Google. To reproduce these experiments, simply run the command: 
```
make reproduce
```
If Python 3 is installed with matplotlib, the results will be plotted in the exp subfolders
as PDF files.

