#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "usage: $0 dataname datafile kmin kmax kinc [GSQK]"
    echo "delete results files if necessary"
    exit
fi



dataname=$1
datafile=$2
kmin=$3
kmax=$4
kinc=$5
algs=$6

data="$datafile"

echo dataname: $dataname
echo "datafile: $data"

#bin='/home/alan/projects/linear-latex/code/max-cov/maxcov'
bin='../../maxcov'
#cmd="/home/alan/projects/linear-latex/code/max-cov/nodes $data"
#nodes=`$cmd`

echo "Algorithms: "
nAlgs=${#algs}
((nAlgs=nAlgs-1))
ctr=0
for a in `seq 0 1 $nAlgs`;
do
    ((ctr=ctr+1))
    chr=${algs:$a:1}
    if [[ "$chr" = "G" ]]; then
	alg[$ctr]='-G'
    fi
    if [[ "$chr" = "S" ]]; then
	alg[$ctr]='-S'
    fi
    if [[ "$chr" = "Q" ]]; then
	alg[$ctr]='-Q'
	c[$ctr]='-c 1'
	d[$ctr]='-d 1'
	
	((ctr=ctr+1))
	alg[$ctr]='-Q'
	c[$ctr]='-c 1'
	d[$ctr]='-d 0.1'
	p[$ctr]='-p'

	((ctr=ctr+1))
	alg[$ctr]='-Q'
	c[$ctr]='-c 2'
	d[$ctr]='-d 0.2'
	p[$ctr]='-p'

	((ctr=ctr+1))
	alg[$ctr]='-Q'
	c[$ctr]='-c 4'
	d[$ctr]='-d 0.4'
	p[$ctr]='-p'

	((ctr=ctr+1))
	alg[$ctr]='-Q'
	c[$ctr]='-c 16'
	d[$ctr]='-d 1.6'
	p[$ctr]='-p'

	((ctr=ctr+1))
	alg[$ctr]='-Q'
	c[$ctr]='-c 256'
	d[$ctr]='-d 25.6'
	p[$ctr]='-p'
    fi
    if [[ "$chr" = "K" ]]; then
	alg[$ctr]='-K'
    fi
    echo ${alg[$ctr]}
done
nAlgs=$ctr

echo "kmin=$kmin"
echo "kmax=$kmax"
echo "kinc=$kinc"
sleep 10

for a in `seq 1 1 $nAlgs`;
do
    for k in `seq $kmin $kinc $kmax`;
    do
	N='1'
	
        extra=""
	
	output="./results${dataname}${alg[$a]}${extra}${c[$a]}${d[$a]}${p[$a]}${e[$a]}km${kmin}kmx${kmax}kinc${kinc}.txt"
	output="$(echo -e "${output}" | tr -d '[:space:]')"
	cmd="$bin -g $data ${alg[$a]} -o $output -k $k -N $N $extra ${c[$a]} ${p[$a]} ${d[$a]} ${e[$a]}"
	echo $cmd
	$cmd
    done
done

