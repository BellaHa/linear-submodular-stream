#!/bin/bash

##### reproduces smallk experiments of maxcover on webGoogle
if [ -f "./web-Google.txt" ]; then
    echo "Skipping fetch..."
else
    wget http://snap.stanford.edu/data/web-Google.txt.gz
    gunzip web-Google.txt.gz
fi
../../preproc web-Google.txt google.bin

rm results*
bash run.bash Google google.bin 100 1000 100 GSQK

## Generate plots as pdf
python3 plot.py v Google 100 1000 100 0
python3 plot.py q Google 100 1000 100 0
python3 plot.py m Google 100 1000 100 0



