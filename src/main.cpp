#include "mygraph.cpp"
#include "algs.cpp"

#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>

using namespace mygraph;
using namespace std;

void print_help() {
  cout << "Options: " << endl;
  cout << "-g <graph filename in binary format>" << endl
       << "-k <cardinality constraint>" << endl
       << "-G [run StandardGreedy]" << endl
       << "-S [run SieveStream++]" << endl
       << "-P [run P-Pass]" << endl
       << "-L [run LTL]" << endl
       << "-Q [run QuickStream]" << endl
       << "-R [run QS + BR]" << endl
       << "-K [run Chakrabarti&Kale]" << endl
       << "-o <outputFileName>" << endl
       << "-N <repetitions>" << endl
       << "-e <accuracy parameter epsilon (default 0.1)>" << endl
       << "-p [Only with -Q, run QuickStream++]" << endl
       << "-t <Tradeoff in error, only applies to P-Pass>" << endl
       << "-c <blocksize, only applies to QuickStream(++)>" << endl
       << "-q [quiet mode]" << endl;
}

void parseArgs( int argc, char** argv, Args& arg ) {
  int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  
  while ((c = getopt( argc, argv, ":g:k:GQKPTBLSRN:Co:e:c:pt:qd:") ) != -1) {
    switch(c) {
    case 'X':
       arg.alg = QSALT;
       break;
    case 'c':
       sarg.assign( optarg );
       arg.c = stoi( sarg );
       break;
    case 't':
       sarg.assign( optarg );
       arg.tradeoff = stod( sarg );
       break;
    case 'e':
       sarg.assign( optarg );
       arg.epsi = stod( sarg );
       break;
    case 'd':
       sarg.assign( optarg );
       arg.delta = stod( sarg );
       break;
    case 'o':
       sarg.assign( optarg );
       arg.outputFileName = sarg;
       break;
    case 'g':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'k':
       sarg.assign( optarg );
       arg.k = stoi( sarg );
       break;
    case 'N':
       sarg.assign( optarg );
       arg.N = stoi( sarg );
       break;
    case 'G':
       arg.alg = SG;
       break;
    case 'Q':
       arg.alg = QS;
       break;
    case 'C':
       arg.alg = R;
       break;
    case 'K':
       arg.alg = CK;
       break;
    case 'B':
       arg.alg = BS;
       break;
    case 'S':
       arg.alg = SS;
       break;
    case 'R':
       arg.alg = BR;
       break;
    case 'L':
       arg.alg = LG;
       break;
    case 'T':
       arg.alg = TG;
       break;
    case 'P':
       arg.alg = PP;
       break;
    case 'p':
       arg.plusplus = true;
       break;
    case 'q':
       arg.quiet = true;
       break;
    case '?':
      print_help();
      exit( 2 );
      break;
    }
  }

  if (arg.quiet) {
     arg.logg.enabled = false;
     arg.g.logg.enabled = false;
  }
}

void readGraph( Args& args ) {
   args.logg( INFO, "Reading graph from file: " + args.graphFileName + "..." );
   args.g.read_bin( args.graphFileName );
   args.logg( INFO, "Input finished.");
   args.logg << "Nodes: " << args.g.n << ", edges: " << args.g.m << endL;
}

void runAlg( Args& args ) {
   size_t N = args.N;
   allResults.init( "obj" );
   allResults.init( "nEvals" );
   allResults.init( "k" );
   allResults.init( "n" );
   allResults.init( "mem" );
   allResults.add( "k", args.k );
   allResults.add( "n", args.g.n );
   
   for (size_t i = 0; i < N; ++i) {
      args.g.logg << "runAlg: Repetition = " << i << endL;
      clock_t t_start = clock();
      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
      switch (args.alg) {
      case QSALT:
	 {
	    args.logg(INFO, "Starting QuickStream (alternate)..." );
	    Qs qs( args );
	    qs.run_alt();
	 }
	 break;
      case R:
	 {
	    args.logg(INFO, "Starting Random..." );
	    Rand rand( args );
	    rand.run();
	 }
	 break;
      case TG:
	 {
	    args.logg(INFO, "Starting ThresholdGreedy..." );
	    Tg tg( args );
	    tg.run();
	 }
	 break;
      case PP:
	 {
	    args.logg(INFO, "Starting P-Pass..." );
	    Pp pp( args );
	    pp.run();
	 }
	 break;
      case LG:
	 {
	    args.logg(INFO, "Starting LazyGreedy..." );
	    Frg lg( args );
	    lg.run();
	 }
	 break;
      case SG:
	 {
	    args.logg(INFO, "Starting StandardGreedy..." );
	    Sg sg( args );
	    sg.run();
	 }
	 break;
      case QS:
	 {
	    args.logg(INFO, "Starting QuickStream..." );
	    Qs qs( args );
	    qs.run();
	 }
	 break;
      case BS:
	 {
	    args.logg(INFO, "Starting Buchbinder,Feldman,Schwartz..." );
	    Bs bs( args );
	    bs.run();
	 }
	 break;

      case SS:
	 {
	    args.logg(INFO, "Starting SieveStreaming++ ..." );
	    Ss ss( args );
	    ss.run();
	 }
	 break;

      case BR:
	 {
	    args.logg(INFO, "Starting QuickStream + BoostRatio ..." );
	    Br br( args );
	    br.run();
	 }
	 break;
      case CK:
	 {
	    args.logg(INFO, "Starting Chakrabarti & Kale ..." );
	    Ck ck( args );
	    ck.run();
	 }
	 break;
      }
   
      
      args.tElapsed = elapsedTime( t_start );
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
      args.wallTime = WallTimeMillis / 1000.0;

      allResults.init( "epsi" );
      allResults.add( "epsi", args.epsi );
   }

 
}


void outputResults( Args& args ) {

   if (args.outputFileName != "") {
      args.g.logg << "Writing output to file: " << args.outputFileName << endL;
      ofstream of( args.outputFileName.c_str(), ofstream::out | ofstream::app );
      allResults.print( of );
   } else {
      //allResults.print( cout, false );
      allResults.print( "obj" );
      cout << ' ';
      allResults.print( "nEvals" );
      
   }
}

int main(int argc, char** argv) {
  Args args;
  parseArgs( argc, argv, args );
  readGraph( args );
  runAlg( args );
  outputResults( args );
}
